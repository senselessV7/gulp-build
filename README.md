My Personal GULP build front-end for Windows
===============================


Downloads for Windows  & Install
-----------------

1. **[Node.js](https://nodejs.org/)**
2. **[GIT](https://git-scm.com/)**
3. **[RubyInstaller](http://rubyinstaller.org/)**

**Общие настройки:**

Запуск обновления системы
```bash
gem update --system
```

Установка SASS 
```bash
gem install sass
```

Установка SASS Compass.
--pre - последняя стабильная версия этого пакета 

```bash
gem install compass --pre
```
__________________________________________________________________________________________
 **Создание исходной дирректории и файловой структуры:**                    

Релизная дирректория                                                        
                                                                            
``` mkdir www ```                                                          
                                                                            
``` cd www```                                                              
                                                                            
``` mkdir app/{css,js} –p```                                               
                                                                            
ключ ```-p``` - создание одновременно и самой дирректории app               
                                                                            
``` cd app``` - исходники                                                  
                                                                            
Создание необходимых дирректорий и файлов                                   
                                                                            
``` touch js/{main.js,plugins.js} css/{style1.css,style2.css} index.html```
                                                                            
__________________________________________________________________________________________
Исходники для SASS Compass 
                                                                            
``` mkdir scss/{_utilities,layout,base,modules} -p```
                                                                            
``` cd scss```
                                                                            
                                                                            
В соответствии с архитектурой SMACSS 
                                                                            
``` touch _utilities/{_index.scss,_typography.scss,_colors.scss,clearfix.scss} layuout/{_index.scss,_footer.scss,_header.scss,_container.scss} base/{_base.scss} index.scss```
                                                                           
__________________________________________________________________________________________
Переход в www                                                               
                                                                            
``` cd ..```                                                               
                                                                            
``` cd ..```                                                               
                                                                            
                                                              
__________________________________________________________________________________________
![Path app dir](https://github.com/senselessV7/gulp-build/blob/master/images/dir.png "Dir")
__________________________________________________________________________________________

**+ необходимые файлы**  

``` touch .bowerrc bower.json package.json .gitignore gulpfile.js``` 

Просмотр файлов:                                                           
                                                                            
``` ls ```   
                                                                            
Просмотр скрытых файлов:                                                     
                                                                            
``` ls -a ```                                                               
                                                                            
или файлов с точкой                                                         
```ls -f``` 

__________________________________________________________________________________________

**Содержание:**


**.bowerrc**
-----------------

```{
	"directory" : "app/bower_components"
}```

__________________________________________________________________________________________

**bower.json** 
-----------------
 
 ```{ "name" : "test-pj" } ```

__________________________________________________________________________________________

**package.json** 
-----------------------
 
 ```{ }```


__________________________________________________________________________________________

**.gitignore** 
------------------
 
 ```
 app/bower
 node_modules
 ```
 
 
__________________________________________________________________________________________
**Установка плагинов** 
=======================

  ``` npm install ```  

Bower: 
-------  

  ``` npm install -g bower ```       
  ``` npm update -g bower  ``` 
  
  
#####Установка jQuery#####

  ``` bower install --save jquery#1.11 ```                       

Gulp: 
--------

``` npm install -g gulp  ```                                          
``` npm install --save-dev gulp``` 

 
#####Наблюдение за файлами#####

``` npm install gulp-watch ```


#####Сборка файлов#####

``` npm install --save-dev gulp-rigger ```

                                                                                          
 В HTML:                                                                  
```                                                                      
//= template/header.html                                                  
//= template/footer.html                                                  
``` 

		   
#####Подключение bower ф-ов:#####

``` npm install --save wiredep ```


 В HTML:                                                                   
```                                                                        
<html>                                                                     
<head>                                                                     
  <!-- bower:css -->                                                       
  <!-- endbower -->                                                        
</head>                                                                    
<body>                                                                     
  <!-- bower:js -->                                                        
  <!-- endbower -->                                                        
</body>                                                                    
</html>                                                                    
```   


#####Чистка 

``` npm install --save-dev gulp-clean ```


#####Конкатенация и собрание ф-ов#####
``` npm install --save-dev gulp-useref ``` 


 В HTML:
 
 ```                                                                       
<html>                                                                     
<head>                                                                     
    <!-- build:css css/combined.css -->                                    
    <link href="css/one.css" rel="stylesheet">                             
    <link href="css/two.css" rel="stylesheet">                             
    <!-- endbuild -->                                                      
</head>                                                                    
<body>                                                                     
    <!-- build:js scripts/combined.js -->                                  
    <script type="text/javascript" src="scripts/one.js"></script>          
    <script type="text/javascript" src="scripts/two.js"></script>          
    <!-- endbuild -->                                                      
</body>                                                                    
</html>                                                                    
 ```
 

#####Автоматическая перезагрузка#####

``` npm install browser-sync ``` 


#####Префиксы#####

``` npm install --save-dev gulp-autoprefixer ``` 


#####Минификация js#####

``` npm install --save-dev gulp-uglify ```


#####Минификация css#####

``` npm install --save-dev gulp-minify-css ``
`

#####Минификация images#####

``` npm install --save-dev gulp-imagemin ```
``` npm install --save-dev gulp-pngmin ``` 


#####Работа с sass compass#####

``` npm install gulp-compass --save-dev ```


__________________________________________________________________________________________
